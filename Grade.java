import java.util.Scanner;

public class Grade {
    public static void main(String[] args) {

            Scanner appScanner = new Scanner(System.in);
            System.out.println("First Name");
            String firstName = appScanner.nextLine() ;
            System.out.println("Last Name");
            String lastName = appScanner.nextLine();
            System.out.println("First Subject Grade");
            double firstGrade = appScanner.nextDouble();
            System.out.println("Second Subject Grade");
            double secondGrade = appScanner.nextDouble();
            System.out.println("Third Subject Grade");
            double thirdGrade = appScanner.nextDouble();

            double averageGrade = (firstGrade + secondGrade + thirdGrade) / 3;
            System.out.println("Good day, " + firstName + " " + lastName + ". Your grade average is " + averageGrade);

    }
}
